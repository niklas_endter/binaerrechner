package com.niklasendter.binaerrechner.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class BinaryDto {

    @NotEmpty
    @Size(min = 4, max = 4)
    @Pattern(regexp = "^[01]+$", message = "Number must be binary!")
    private String binary;

    private String decimal;

    public BinaryDto(String binary, String decimal) {
        this.binary = binary;
        this.decimal = decimal;
    }

    public BinaryDto() {
    }

    public String getBinary() {
        return binary;
    }

    public void setBinary(String binary) {
        this.binary = binary;
    }

    public String getDecimal() {
        return decimal;
    }

    public void setDecimal(String decimal) {
        this.decimal = decimal;
    }
}
