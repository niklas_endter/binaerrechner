package com.niklasendter.binaerrechner.controller;

import com.niklasendter.binaerrechner.dto.BinaryDto;
import com.niklasendter.binaerrechner.service.BinaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@Controller
public class BinaryController {

    private final BinaryService binaryService;

    @Autowired
    public BinaryController(BinaryService binaryService) {
        this.binaryService = binaryService;
    }

    @GetMapping
    public String index(Model model){
        model.addAttribute("binaryDto", new BinaryDto());
        return "index";
    }

    @PostMapping
    public String decimalConversion(@Valid @ModelAttribute BinaryDto binaryDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "index";
        }
        model.addAttribute("decimal", binaryService.binaryToDecimal(binaryDto.getBinary()));
        return "result";
    }
}
