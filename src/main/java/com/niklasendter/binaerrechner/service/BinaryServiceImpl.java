package com.niklasendter.binaerrechner.service;

import org.springframework.stereotype.Service;

@Service
public class BinaryServiceImpl implements BinaryService{
    @Override
    public Integer binaryToDecimal(String binary) {
        return Integer.parseInt(binary,2);
    }
}
