package com.niklasendter.binaerrechner.service;

public interface BinaryService {
    Integer binaryToDecimal(String binary);
}
